#include "network.h"
#include "listener.h"
#include "gameprovider.h"
#include "gamewindow.h"

#include <QObject>

    /**
     * @brief Network::Network runs Listener thread.
     * @param parent parent widget.
     * @param myName user's name.
     */
    Network::Network(QObject *parent, QString myName) : QObject(parent), myName(myName) {
        Listener *listener = new Listener();
        listener->moveToThread(&listenThread);
        listenThread.setObjectName("Listener");
        connect(&listenThread, &QThread::finished, listener, &QObject::deleteLater);
        connect(this, &Network::startListener, listener, &Listener::startListening);
        connect(this, &Network::stopListener, listener, &Listener::stopListening);
        connect(listener, &Listener::started, this, &Network::handleListenerStarted);
        connect(listener, &Listener::stopped, this, &Network::handleListenerStopped);
        connect(listener, &Listener::connectionRequestReceived, this, &Network::handleGameRequest);
        connect(listener, &Listener::errorOccured, this, &Network::handleListenerError);
        listenThread.start();
    }

    /**
     * @brief Network::~Network stop listening thread before destruction.
     */
    Network::~Network() {
        listenThread.quit();
        listenThread.wait();
    }

    /**
     * @brief Network::handleListenerStarted saves listening port when receives confirmation signal from Listener. Propagates signal to main window.
     * @param port port on which Listener is listening.
     */
    void Network::handleListenerStarted(QString port) {
        listeningPort = port;
        isListening = true;
        emit listenerStarted(port);
    }

    /**
     * @brief Network::handleListenerStopped updates Listener status and propagates signal to main window.
     */
    void Network::handleListenerStopped() {
        isListening = false;
        emit listenerStopped();
    }

    /**
     * @brief Network::handleStopListener unblocks listening thread using single shot connection, when listener stop requested from user.
     */
    void Network::handleStopListener() {
        if (isListening) {
            emit stopListener();
            Socket socket;
            try {
                socket.openClientSocket("localhost", listeningPort.toLocal8Bit().data());
                socket.close();
            }  catch (const std::runtime_error& e) {
                emit errorOccured("Cannot connect to listener: " + QString::fromStdString(e.what()));
            }
        }
    }

    /**
     * @brief Network::handleListenerStopped updates Listener status and propagates signal to main window.
     */
    void Network::handleListenerError() {
        emit errorOccured("Listener stopped because of error.");
        isListening = false;
        emit listenerStopped();
    }

    /**
     * @brief Network::startGameThread starts new game thread.
     * @param provider game provider which holds game state and manages communication.
     */
    void Network::startGameThread(GameProvider *provider) {
        QThread *newThread = new QThread();
        newThread->setObjectName("GameProvider Thread");
        createdThreads.append(newThread);
        provider->moveToThread(newThread);
        connect(newThread, &QThread::started, provider, &GameProvider::runGame);
        connect(newThread, &QThread::finished, provider, &QObject::deleteLater);
        connect(provider, &GameProvider::establishedNewConnection, this, &Network::establishedNewConnection);
        newThread->start();
    }

    /**
     * @brief Network::establishNewConnection open new connection on given opponent's adress and port.
     * @param ipAdress opponent's address.
     * @param port opponent's port.
     */
    void Network::openNewConnection(QString ipAdress, QString port) {
        Socket otherSocket;
        try {
            otherSocket.openClientSocket(ipAdress.toLocal8Bit().data(), port.toLocal8Bit().data());
        }  catch (const std::runtime_error& e) {
            emit errorOccured(QString::fromStdString(e.what()));
        }
        GameProvider *provider = new GameProvider(otherSocket, GameProvider::ConnectionRole::INITIATED, myName);
        startGameThread(provider);
    }

    /**
     * @brief Network::handleGameRequest handle game request from opponent.
     * @param opponentSocket opponent's socket.
     */
    void Network::handleGameRequest(Socket opponentSocket) {
        GameProvider *provider = new GameProvider(opponentSocket, GameProvider::ConnectionRole::ACCEPTED, myName);
        startGameThread(provider);
    }
