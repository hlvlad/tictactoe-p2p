#pragma once

#include "board.h"
#include <QDialog>
#include <QPainter>


class GameWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GameWidget(QWidget *parent = nullptr);
    GameWidget(QWidget *parent, bool isInitiator);
    ~GameWidget();
    enum GameState {Victory = 1, Tie = 0, Defeat = -1, Play = 2};

    void PlayAi();

private:
    enum : char {Empty = '-', Cross = 'X', Nought = 'O'};

    QRect cellRect(int row, int col) const;
    int cellWidth() const {return width() / 3;}
    int cellHeight() const {return height() / 3;}
    int turnNumber;
    int myFigure;
    bool isMyTurn();

    Board board;

    bool makeMoveOnBoard(char pos);
    void toggleMyFigure();
protected:
    void mousePressEvent(QMouseEvent *e) override;
    void paintEvent(QPaintEvent *e) override;

signals:
    void makeMove(char pos);
    void gameEnded(int gameState);
    void gameStatusChanged(QString status);
    void showMessage(QString message);

public slots:
    void moveReceived(char pos);
};

