#include "messagehandler.h"

//constexpr int LENGTH_FIELD_SIZE = 1;

MessageHandler::MessageHandler(Socket socket) : socket(socket) { }


/**
 * @brief MessageHandler::sendName sends user's name to opponent.
 * @param name user's name to send.
 */
void MessageHandler::sendName(QString name) {
    char buf[64];
    int payloadSize = name.size() + 1;
    buf[0] = NAME;
    buf[1] = payloadSize;
    memcpy(buf+2, name.toStdString().c_str(), payloadSize);
    socket.sendAll(buf, 2 + payloadSize);
}

/**
 * @brief MessageHandler::sendMove sends user's move to opponent.
 * @param position position where new piece was set.
 */
void MessageHandler::sendMove(const char position) {
    char buf[2];
    buf[0] = MOVE;
    buf[1] = position;
    socket.sendAll(buf, 2);
}

/**
 * @brief MessageHandler::sendSignal sends short one byte message to opponent.
 * @param signal message code to send.
 */
void MessageHandler::sendSignal(const char signal) {
    socket.sendAll(&signal, 1);
}

/**
 * @brief MessageHandler::receiveMessageType receives one byte message code.
 * @return received message code.
 */
char MessageHandler::receiveMessageType() {
    unsigned char messageType;
    socket.receiveAll(&messageType, 1);
    return messageType;
}

/**
 * @brief MessageHandler::receiveName receives opponent's name.
 * @return received opponent's name.
 */
QString MessageHandler::receiveName() {
    unsigned char buf[64];
    unsigned char payloadSize;
    socket.receiveAll(&payloadSize, 1);
    socket.receiveAll(buf, payloadSize);
    return QString::fromLocal8Bit((const char*)buf);
}

/**
 * @brief MessageHandler::receiveMove receives opponent's move.
 * @return position where opponent's piece was placed. Value from 0 to 8.
 */
char MessageHandler::receiveMove() {
    unsigned char pos;
    socket.receiveAll(&pos, 1);
    return pos;
}
