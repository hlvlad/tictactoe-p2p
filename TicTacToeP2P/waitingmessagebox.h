#ifndef WAITINGMESSAGEBOX_H
#define WAITINGMESSAGEBOX_H

#include <QMessageBox>
#include <QObject>

class WaitingMessageBox : public QMessageBox
{
    Q_OBJECT

public:
    WaitingMessageBox(QWidget *parent = nullptr);
public slots:
    void handleAccept();
    void handleReject();
};

#endif // WAITINGMESSAGEBOX_H
