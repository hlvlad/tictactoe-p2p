#include "gameprovider.h"
#include "gamewindow.h"
#include "messagehandler.h"

#include <QCoreApplication>
#include <QString>
#include <csignal>

/**
 * @brief GameProvider::GameProvider default constructor.
 * @param otherSocket opponent's socket.
 * @param role role of the client. May be either INITIATED or ACCEPTED.
 * @param myName name provided by the user.
 */
GameProvider::GameProvider(Socket otherSocket, ConnectionRole role, QString myName) : connectionRole(role), myName(myName), messageHandler(otherSocket) { }

GameProvider::~GameProvider() {
    opponentSocket.close();
}

/**
 * @brief GameProvider::initiateGame exchange names with opponent. Send (if initiatied) or Accept/Reject new game request. Start game loop.
 */
void GameProvider::runGame()
{
    try {
        messageHandler.sendName(myName);
        isMyTurn = connectionRole == INITIATED;
    } catch (const std::runtime_error& e) {
        emit errorOccured(QString::fromStdString(e.what()));
    }
    // Receive name
    receiveMessage();

    // Receive accept
    if(connectionRole == INITIATED) {
        receiveMessage();
    } else {
        // Get user decision from window
        QEventLoop loop;
        connect(this, &GameProvider::unblockWaiting, &loop, &QEventLoop::quit);
        loop.exec();
    }
    while(!isDone) {
        if(!isMyTurn) {
            receiveMessage();
        }
        QEventLoop loop;
        connect(this, &GameProvider::unblockWaiting, &loop, &QEventLoop::quit);
        loop.exec();
    }
    if (closeReason != "") {
        emit showMessage(closeReason);
    }
}

/**
 * @brief GameProvider::acceptGameRequest Notify opponent about user decision. Slot is executed if user accepted game request from opponent.
 */
void GameProvider::acceptGameRequest()
{
    try {
       messageHandler.sendSignal(MessageHandler::ACCEPT);
    } catch (const std::runtime_error& e) {
        emit errorOccured(QString::fromStdString(e.what()));
    }
    emit unblockWaiting();
}

/**
 * @brief GameProvider::acceptGameRequest Notify opponent about user decision. Slot is executed if user rejected game request from opponent.
 */
void GameProvider::rejectGameRequest() {
    try {
        messageHandler.sendSignal(MessageHandler::REJECT);
    } catch (const std::runtime_error& e) {
        emit errorOccured(QString::fromStdString(e.what()));
    }
    finalize();
}

/**
 * @brief GameProvider::interruptConnection drop connection between players.
 */
void GameProvider::dropConnection()
{
    try {
        messageHandler.sendSignal(MessageHandler::DISCONNECT);
    } catch (const std::runtime_error& e) {
        emit errorOccured(QString::fromStdString(e.what()));
    }
    emit connectionClosed();
    finalize();
}

/**
 * @brief GameProvider::finalize exit game loop, close opponent's socket, exit event waiting loop.
 */
void GameProvider::finalize() {
    isDone = true;
    try {
        opponentSocket.close();
    }  catch (const std::runtime_error& e) {
        emit errorOccured(QString::fromStdString(e.what()));
    }
    emit unblockWaiting();
}

/**
 * @brief GameProvider::sendMove send move to opponent
 * @param position position of new piece.
 */
void GameProvider::sendMove(char position) {
    try {
       messageHandler.sendMove(position);
       isMyTurn = false;
    } catch (const std::runtime_error& e) {
        emit errorOccured(QString::fromStdString(e.what()));
    }
    emit unblockWaiting();
}

/**
 * @brief GameProvider::receiveMessage handle all messages from opponent. First recognises message type, then executes appropriate receiver.
 */
void GameProvider::receiveMessage() {
    char messageType = messageHandler.receiveMessageType();
    switch (messageType) {
    case MessageHandler::MOVE: {
        try {
           char pos = messageHandler.receiveMove();
           emit moveReceived(pos);
           isMyTurn = true;
        } catch (const std::runtime_error& e) {
            emit errorOccured(QString::fromStdString(e.what()));
        }
        break;
    }
    case MessageHandler::NAME: {
        try {
            opponentName = messageHandler.receiveName();
        } catch (const std::runtime_error& e) {
            emit errorOccured(QString::fromStdString(e.what()));
        }
        emit establishedNewConnection(this, opponentName);
        break;
    }
    case MessageHandler::DISCONNECT: {
        emit showMessage(opponentName + " disconnected from game.");
        emit connectionClosed();
        finalize();
        break;
    }
    case MessageHandler::ACCEPT: {
        emit opponentAcceptedRequest();
        break;
    }
    case MessageHandler::REJECT: {
        emit opponentRejectedRequest();
        break;
    }
    }
}

