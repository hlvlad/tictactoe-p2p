#include "socket.h"

#include <cstdlib>
#include <stdexcept>
#include <cstring>
#include <unistd.h>

#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>


Socket::Socket() {}

Socket::Socket(int socketFD) : socketFD(socketFD) {}


/**
 * @brief Socket::openClientSocket connects client socket to givent address.
 * @param ip address to cennect.
 * @param port port to cennect.
 */
void Socket::openClientSocket(const char* ip, const char* port) {

    int status, sockfd;
    struct addrinfo hints, *servinfo, *p;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(ip, port, &hints, &servinfo)) != 0) {
        throw std::runtime_error("Invalid IP address or port: " + std::string(gai_strerror(status)));
    }


    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
            perror("openServerSocket: socket ");
            continue;
        }

        if(::connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            throw std::runtime_error("Could not connect to client.");
          }

        break;
    }


    if (p == NULL) {
        throw std::runtime_error("Failed to connect to client.");
    }
    address = *p;

    freeaddrinfo(servinfo);

    if(socketFD != -1) {
        if(::close(socketFD) == -1) {
            throw std::runtime_error("Failed to close socket.");
        }
    }
    socketFD=sockfd;
}


/**
 * @brief Socket::openServerSocket open listening socket on given port.
 * @param port port number to listen.
 */
void Socket::openServerSocket(const char* port) {
    int status, sockfd, yes = 1;
    struct addrinfo hints, *servinfo, *p;
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    char portStr[6];

//    int portNum = strtol(port, NULL, 10);
    for(int i = 0; i < 5; i++) {

        if ((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
            throw std::runtime_error("Invalid IP address or port: " + std::string(gai_strerror(status)));
        }

        for(p = servinfo; p != NULL; p = p->ai_next) {
             address = *p;

             if ((sockfd = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
                  perror("openServerSocket: socket ");
                  continue;
             }

             if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
                  perror("openServer: setsockopt");
                  throw std::runtime_error("Could not set SO_REUSEADDR.");
             }

             if(::bind(sockfd, p->ai_addr, p->ai_addrlen) < 0) {
                  ::close(socketFD);
                  continue;
             }

             break;
        }


        if (p != NULL) break;

    }

    if (p == NULL) {
        throw std::runtime_error("Failed to bind server socket.");
    }
    address = *p;

    freeaddrinfo(servinfo);

    if(::listen(sockfd, 64) == -1) {
        if(::close(socketFD) == -1) {
            throw std::runtime_error("Failed to close socket.");
        }
        throw std::runtime_error("Failed to start listening server socket.");
    }


    if(socketFD != -1 && socketFD != sockfd) {
        if(::close(socketFD) == -1) {
            throw std::runtime_error("Failed to close socket.");
        }
    }
    socketFD=sockfd;
}

/**
 * @brief Socket::getPortString get port number in human readable format.
 * @return port number string.
 */
QString Socket::getPortString() {
    int port;
    if(address.ai_family == AF_INET) {
        struct sockaddr_in *ipv4 = (struct sockaddr_in*)address.ai_addr;
        port = ntohs(ipv4->sin_port);
    } else {
        struct sockaddr_in6 *ipv6 = (struct sockaddr_in6*)address.ai_addr;
        port = ntohs(ipv6->sin6_port);
    }
    return QString::number(port);
}

/**
 * @brief Socket::getsIpString get ip address in human readable format.
 * @return ip address string.
 */
QString Socket::getIpString() {
    char ipstr[INET6_ADDRSTRLEN];
    if(address.ai_family == AF_INET) {
        struct sockaddr_in *ipv4 = (struct sockaddr_in*)address.ai_addr;
        inet_ntop(address.ai_family, &ipv4->sin_addr, ipstr, sizeof ipstr);
    } else {
        struct sockaddr_in6 *ipv6 = (struct sockaddr_in6*)address.ai_addr;
        inet_ntop(address.ai_family, &ipv6->sin6_addr, ipstr, sizeof ipstr);
    }
    return QString(ipstr);
}

/**
 * @brief Socket::getAdressString get full address (ip + port) in human readable format.
 * @return full address string.
 */
QString Socket::getAdressString() {
    return getIpString() + ":" + getPortString();
}

/**
 * @brief Socket::accept accepts new connection.
 * @return socket with new connection.
 */
Socket Socket::accept() {
    int otherSocket = ::accept(socketFD, NULL, NULL);
    if(otherSocket == -1) {
        throw std::runtime_error("Failed to accept incoming connection: " + std::string(strerror(errno)));
    }
    return Socket(otherSocket);
}

/**
 * @brief Socket::sendAll sends len bytes from buf buffer.
 * @param buf buffer where data is stored.
 * @param len length of data in bytes.
 * @return number of sent bytes.
 */
int Socket::sendAll(const char *buf, int len)
{
    if(socketFD == -1)
        return 0;

    int total = 0;        // how many bytes we've sent
    int n;

    while(total < len) {
        n = send(socketFD, buf+total, len-total, 0);
        if (n == -1) {
            throw std::runtime_error("Failed to send data.");
        }
        total += n;
    }

    if(total == 0) {
        socketFD = -1;
    }

    return total; // return -1 on failure, 0 on success
}

/**
 * @brief Socket::receiveAll receive len bytes into buf buffer.
 * @param buf buffer where received data should be stored.
 * @param len expected num of received bytes in bytes.
 * @return
 */
int Socket::receiveAll(unsigned char *buf, int len) {

    int total = 0;
    int n;

    while(total < len) {
        n = recv(socketFD, buf+total, len-total, 0);
        if (n == -1) {
            throw std::runtime_error("Failed to receive data.");
        }
        total += n;
    }

    if(total == 0) {
        socketFD = -1;
    }

    return total;
}

/**
 * @brief Socket::close close socket, also shutdown berore to unblock all blocking operations.
 */
void Socket::close() {
    if (socketFD == -1) {
        return;
    }
    int status = ::shutdown(socketFD, SHUT_RDWR);
    if (status == -1) {
        throw std::runtime_error("Failed to shutdown socket: " + std::string(strerror(errno)));
    }
    status = ::close(socketFD);
    if (status == -1) {
        throw std::runtime_error("Failed to close socket: " + std::string(strerror(errno)));
    }

    socketFD = -1;
}

