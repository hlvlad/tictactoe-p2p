#include "board.h"

Board::Board() : boardState(None) { }

/**
 * @brief Board::clear Clears board representation and resets board state.
 */
void Board::clear()
{
    for(int i = 0; i < 9; i++)
        board[i] = Empty;

    boardState.state = None;
}

/**
 * @brief Board::setSquare sets square at position @param position with a value @param value.
 * @param position destination position.
 * @param type value which will be set.
 */
void Board::setSquare(int position, int type)
{
    board[position] = type;
}

/**
 * @brief Board::unsetSquare deletes piece from position @param position.
 * @param position position of deleted element.
 */
void Board::unsetSquare(int position)
{
    board[position] = Empty;
}

/**
 * @brief Board::getSquare returns value at given position on board.
 * @param position position of returned element.
 * @return value at position @param position.
 */
int Board::getSquare(int position)
{
    return board[position];
}

/**
 * @brief Board::getState gets @param BoardState of the board.
 * @return board state calculated on last evaluate() call.
 */
BoardState Board::getState() {
    return boardState;
}

/**
 * @brief Board::evaluate calculates current board state.
 */
void Board::evaluate()
{
    for(int i = 0; i < 9; i+=3) {
        if (board[i] != Empty && board[i] == board[i+1] && board[i+1] == board[i+2]) {
            boardState = BoardState(WinRow, i);
            return;
        }
    }
    for(int i = 0; i < 3; ++i) {
        if (board[i] != Empty && board[i] == board[i+3] && board[i+3] == board[i+6]) {
            boardState = BoardState(WinColumn, i);
            return;
        }
    }
    if (board[0] != Empty && board[0] == board[4] && board[4] == board[8]) {
        boardState = BoardState(WinDiagonal, 0);
        return;
    }
    if (board[2] != Empty && board[2] == board[4] && board[4] == board[6]) {
        boardState = BoardState(WinDiagonal, 1);
        return;
    }
    for(int i = 0; i < 9; ++i) {
        if (board[i] == Empty) {
            boardState = BoardState(None);
            return;
        }
    }
    boardState = BoardState(Tie);
}

