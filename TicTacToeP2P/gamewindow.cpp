#include "gamewidget.h"
#include "gamewindow.h"

#include <QFormLayout>
#include <QMessageBox>
#include <QObject>
#include <QPushButton>

/**
 * @brief GameWindow::GameWindow game window. Contains score, status and game board.
 * @param parent parent widget.
 * @param isMyTurn determines whether first turn will be user's.
 * @param opponentName name of the opponent.
 */
GameWindow::GameWindow(QWidget *parent, bool isMyTurn, QString opponentName) : QDialog(parent), opponentName(opponentName)
{
    QVBoxLayout *mainLayout = new QVBoxLayout;

    QFormLayout *scoreLayout = new QFormLayout;
    scoreGroupBox = new QGroupBox(tr("Score"), this);
    scoreGroupBox->setLayout(scoreLayout);

    myScoreLabel = new QLabel("0", scoreGroupBox);
    opponentScoreLabel = new QLabel("0", scoreGroupBox);
    scoreLayout->addRow("Me:", myScoreLabel);
    scoreLayout->addRow("Opponent:", opponentScoreLabel);

    QFormLayout *statusLayout = new QFormLayout;
    QString gameStatus = isMyTurn ? "Your turn." : "Opponent's turn.";
    gameStatusLabel = new QLabel(gameStatus, this);
    statusLayout->addRow("Game status:", gameStatusLabel);

    gameWidget = new GameWidget(this, isMyTurn);
    connect(gameWidget, &GameWidget::showMessage, this, &GameWindow::showMessage);
    connect(gameWidget, &GameWidget::makeMove, this, &GameWindow::makeMove);
    connect(this, &GameWindow::moveReceived, gameWidget, &GameWidget::moveReceived);
    connect(gameWidget, &GameWidget::gameStatusChanged, this, &GameWindow::setGameStatus);
    connect(gameWidget, &GameWidget::gameEnded, this, &GameWindow::gameEnded);

    quitButton = new QPushButton(tr("&Quit"), this);
    connect(quitButton, &QAbstractButton::clicked, this, &GameWindow::closeGame);

    QHBoxLayout *quitButtonBox = new QHBoxLayout;
    quitButtonBox->addStretch();
    quitButtonBox->addWidget(quitButton);

    mainLayout->addWidget(scoreGroupBox);
    mainLayout->addLayout(statusLayout);
    mainLayout->addWidget(gameWidget);
    mainLayout->addLayout(quitButtonBox);

    setLayout(mainLayout);
    setWindowTitle("Game with Opponent.");
}

/**
 * @brief GameWindow::gameStarted inform user that hame have started.
 */
void GameWindow::gameStarted()
{
    QMessageBox::information(this, "Info" , "Game started");
}

/**
 * @brief GameWindow::gameEnded display appropriate message when current game have resolved. Increment score accordingly.
 * @param gameState wariable which determines resolved game state. Can be Play, Victory, Defeat or Tie.
 */
void GameWindow::gameEnded(int gameState)
{
    switch (gameState) {
    case GameWidget::Victory:
        showMessage("You win!");
        setGameStatus("You won!");
        myScore+=2;
        myScoreLabel->setText(QString::number(myScore));
        break;
    case GameWidget::Defeat:
        showMessage("You lose.");
        setGameStatus("You lose.");
        opponentScore+=2;
        opponentScoreLabel->setText(QString::number(opponentScore));
        break;
    case GameWidget::Tie:
        showMessage("I't tie.");
        setGameStatus("Tie.");
        myScore++;
        opponentScore++;
        myScoreLabel->setText(QString::number(myScore));
        opponentScoreLabel->setText(QString::number(opponentScore));
        break;
    default:
        //should never reach here
        break;
    }
}

/**
 * @brief GameWindow::showMessage display given message in QMessageBox.
 * @param message message to display.
 */
void GameWindow::showMessage(QString message)
{
    QMessageBox::information(this, "Info" , message);
}

/**
 * @brief GameWindow::fatalError notify user that fatal error occured.
 * @param errorMessage error message to display.
 */
void GameWindow::fatalError(QString errorMessage)
{
    QMessageBox::critical(this, "Error", errorMessage);
    this->done(1);
}

/**
 * @brief GameWindow::closeWindow close game window.
 */
void GameWindow::closeWindow() {
    this->done(0);
}


/**
 * @brief GameWindow::accept display confirmation dialog when trying to exit.
 */
void GameWindow::accept() {
    if(QMessageBox::Yes == QMessageBox::question(this, "Exit confirmation", "Game will be closed. Continue?")) {
        QDialog::accept();
    }
}

/**
 * @brief GameWindow::closeGame close window and stop game at GameProvider.
 */
void GameWindow::closeGame()
{
    emit gameStopped();
    this->done(0);
}

/**
 * @brief GameWindow::setGameStatus display current game status in coresponding place.
 * @param status status string to display.
 */
void GameWindow::setGameStatus(QString status)
{
    gameStatusLabel->setText(status);
}
