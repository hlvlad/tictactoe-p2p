#include "gamewidget.h"
#include "board.h"
#include <QPainter>
#include <QMouseEvent>
#include <QMessageBox>


/**
 * @brief GameWidget::GameWidget widget with Tic Tac Toe board.
 * @param parent parent widget.
 * @param isInitiator shows whether user initiated connection.
 */
GameWidget::GameWidget(QWidget *parent, bool isInitiator) : QWidget(parent), turnNumber(0)
{
    this->setWindowTitle("Tic Tac Toe Network");
    this->setFixedSize(300, 300);
    myFigure = isInitiator ? Board::Cross : Board::Nought;
    board.clear();
}

GameWidget::~GameWidget() {  }

/**
 * @brief GameWidget::cellRect get cell rectangle from board at given location.
 * @param row row index where cell is located.
 * @param col column index where cell is located.
 * @return QRect representing cell rectangle.
 */
QRect GameWidget::cellRect(int row, int col) const
{
    const int HMargin = width() / 30;
    const int VMargin = height() / 30;
    return QRect(col * cellWidth() + HMargin,
                 row * cellHeight() + VMargin,
                 cellWidth() - 2 * HMargin,
                 cellHeight() - 2 * VMargin);
}

/**
 * @brief GameWidget::paintEvent draw board, pieces and crossing line in case of win.
 * @param e painting event.
 */
void GameWidget::paintEvent(QPaintEvent *e)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    painter.setPen(QPen(Qt::black, 1));
    painter.drawLine(cellWidth(), 0, cellWidth(), height());
    painter.drawLine(2 * cellWidth(), 0, 2 * cellWidth(), height());
    painter.drawLine(0, cellHeight(), width(), cellHeight());
    painter.drawLine(0, 2 * cellHeight(), width(), 2 * cellHeight());

    painter.setPen(QPen(Qt::darkBlue, 6));

    for(int pos = 0; pos < 9; pos++) {
        QRect cell = cellRect(pos / 3, pos % 3);
        if (board.getSquare(pos) == Cross) {
            painter.drawLine(cell.topLeft(), cell.bottomRight());
            painter.drawLine(cell.topRight(), cell.bottomLeft());
        } else if (board.getSquare(pos) == Nought)  {
            painter.drawEllipse(cell);
        }
    }

    painter.setPen(QPen(Qt::red, 4));

    int gameState = Play;
    BoardState boardState = board.getState();
    switch(boardState.state) {
    case Board::WinRow: {
        int y = cellRect(boardState.index, 0).center().y();
        painter.drawLine(0, y, width(), y);
        gameState = !isMyTurn() ? Victory : Defeat;
        break;
    }
    case Board::WinColumn: {
        int x = cellRect(0, boardState.index).center().x();
        painter.drawLine(x, 0, x, height());
        gameState = !isMyTurn() ? Victory : Defeat;
        break;
    }
    case Board::WinDiagonal: {
        if(boardState.index == 0) {
            painter.drawLine(0, 0, width(), height());
        } else if (boardState.index == 1) {
            painter.drawLine(width(), 0, 0, height());
        }
        gameState = !isMyTurn() ? Victory : Defeat;
        break;
    }
    case Board::Tie: {
        gameState = Tie;
        break;
    }
    default:
        //should never reach here
        break;
    }

    if(gameState != Play) {
        emit gameEnded(gameState);
        board.clear();
        turnNumber = 0;
        toggleMyFigure();
        update();
    }
}

/**
 * @brief GameWidget::isMyTurn check whether it's user's turn now.
 * @return true if it's users turn, false otherwise.
 */
bool GameWidget::isMyTurn() {
    return (turnNumber % 2 == 0 && myFigure == Board::Cross) ||
           (turnNumber % 2 == 1 && myFigure == Board::Nought);
}

/**
 * @brief GameWidget::toggleMyFigure change user's figure. Changes Cross to Nought and vice versa.
 */
void GameWidget::toggleMyFigure() {
    if(myFigure == Board::Cross)
        myFigure = Board::Nought;
    else
        myFigure = Board::Cross;
}

/**
 * @brief GameWidget::makeMoveOnBoard set user's piece at given position on board.
 * @param position - position where piece should be placed. Value from 0 to 8.
 * @return true if position was empty and piece was set, false otherwise.
 */
bool GameWidget::makeMoveOnBoard(char position) {
    bool didMove = false;
    if(board.getSquare(position) == Board::Empty) {
        if (turnNumber % 2 == 0) {
            board.setSquare(position, Board::Cross);
        } else {
            board.setSquare(position, Board::Nought);
        }
        board.evaluate();
        didMove = true;
     }
    return didMove;
}

/**
 * @brief GameWidget::moveReceived process received move from opponent. Set opponent's piece at given position if it's empty.
 * @param position position where opponent's piece should be placed.
 */
void GameWidget::moveReceived(char position) {
    if(makeMoveOnBoard(position)) {
        emit gameStatusChanged("Your turn.");
        update();
        turnNumber++;
    }
}

/**
 * @brief GameWidget::mousePressEvent process mouse click event. If user clicked on empty cell and it's user's turn, set piece in that cell.
 * @param e mouse click event.
 */
void GameWidget::mousePressEvent(QMouseEvent *e)
{
    if(isMyTurn()) {
        for(int pos = 0; pos < 9; ++pos) {
            QRect cell = cellRect(pos/3, pos%3);
            if (cell.contains(e->pos())) {
                if(makeMoveOnBoard(pos)) {
                    emit makeMove(pos);
                    emit gameStatusChanged("Opponent's turn.");
                    update();
                    turnNumber++;
                }
            }
        }
    }
}


