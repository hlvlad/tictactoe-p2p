#ifndef SOCKET_H
#define SOCKET_H

#include <QString>
#include <netdb.h>


class Socket
{
public:
    Socket();
    QString getAdressString();
    QString getIpString();
    QString getPortString();

    void openClientSocket(const char *ip, const char *port);
    void openServerSocket(const char *port);

    Socket(int socketFD);
    Socket accept();
    int sendAll(unsigned char *buf, int len);
    int sendAll(const char *buf, int len);
    int receiveAll(unsigned char *buf, int len);
    void close();
private:
    int socketFD = -1;
    struct addrinfo address;
};

#endif // SOCKET_H
