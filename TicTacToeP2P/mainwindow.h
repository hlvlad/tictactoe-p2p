#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "network.h"
#include "gamewidget.h"
#include "gamewindow.h"

#include <QDialog>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class MainWindow : public QDialog
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

    enum ListenStatus {
        Listening,
        Down
    };
    Q_ENUM(ListenStatus)

    void closeEvent(QCloseEvent *event) override;
public slots:
    void handleListenClick();
    void handleListenerStarted(QString address);
    void handleListenerStopped();
    void showErrorMessage(QString errorMessage);
    void handleNewConnection(GameProvider *provider, QString opponentName);
    void associateWindowWithGame(GameProvider *provider, QString opponentName);

signals:
    void listenerStartRequested(QString port);
    void listenerStopRequested();
    void newConnectionRequested(QString ip, QString port);
    void userAcceptedGameRequest();
    void userRejectedGameRequest();
    void stopAllGamesRequested();

private:
    Network *network;
    ListenStatus listenStatus = Down;
    QString myName = "ProGamer";

    QLabel *myNameLabel;

    QGroupBox *listenGroupBox;
    QLabel *listenStatusLabel;
    QLineEdit *listenPortEdit;
    QPushButton *startListenButton;
    QPushButton *stopListenButton;

    QGroupBox *connectGroupBox;
    QLineEdit *connectAddressEdit;
    QLineEdit *connectPortEdit;
    QPushButton *connectButton;
    QPushButton *openGameButton;

    void connectToOpponent();
    void setDefaultFormValues();
};

#endif // MAINWINDOW_H
