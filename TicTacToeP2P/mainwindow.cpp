﻿#include "mainwindow.h"
#include "gamewidget.h"
#include "gamewindow.h"
#include "waitingmessagebox.h"
#include "gamerequestmessagebox.h"

#include <QCloseEvent>
#include <QFormLayout>
#include <QMessageBox>
#include <QPushButton>


/**
 * @brief MainWindow::MainWindow main program window. Controls listening and connecting to opponents. Also assotiates new game windows with coresponding threads.
 */
MainWindow::MainWindow()
{
    qRegisterMetaType<Socket>("Socket");

    QVBoxLayout *mainLayout = new QVBoxLayout;

    myNameLabel = new QLabel(myName, this);

    // Listen GroupBox
    QVBoxLayout *listenLayout = new QVBoxLayout;
    listenGroupBox = new QGroupBox(tr("Listening configuration"), this);
    listenGroupBox->setLayout(listenLayout);

    listenStatusLabel = new QLabel(listenGroupBox);
    listenStatusLabel->setText(QVariant::fromValue(listenStatus).toString());
    listenPortEdit = new QLineEdit;
    QFormLayout *listenForm = new QFormLayout;
    listenForm->addRow("Listening status:", listenStatusLabel);
    listenForm->addRow("Enter listening port", listenPortEdit);

    startListenButton = new QPushButton("Listen", listenGroupBox);
    connect(startListenButton, &QAbstractButton::clicked, this, &MainWindow::handleListenClick);
    stopListenButton = new QPushButton("Stop", listenGroupBox);
    connect(stopListenButton, &QAbstractButton::clicked, this, &MainWindow::listenerStopRequested);
    stopListenButton->setDisabled(true);

    QHBoxLayout *buttonBox = new QHBoxLayout;
    buttonBox->addStretch();
    buttonBox->addWidget(startListenButton, 0);
    buttonBox->addWidget(stopListenButton, 0);
    listenLayout->addLayout(listenForm);
    listenLayout->addLayout(buttonBox);


//    // Connect GroupBox
    QVBoxLayout *connectLayout = new QVBoxLayout;
    connectGroupBox = new QGroupBox(tr("Connect to opponent"));
    connectGroupBox->setLayout(connectLayout);

    QFormLayout *connectForm = new QFormLayout;
    connectAddressEdit = new QLineEdit;
    connectPortEdit = new QLineEdit;
    connectForm->addRow("Enter opponent's address", connectAddressEdit);
    connectForm->addRow("Enter opponent's port", connectPortEdit);

    connectButton = new QPushButton(tr("Connect"));
    connect(connectButton, &QAbstractButton::clicked, this, &MainWindow::connectToOpponent);

    QHBoxLayout *connectButtonBox = new QHBoxLayout;
    connectButtonBox->addStretch();
    connectButtonBox->addWidget(connectButton);

    connectLayout->addLayout(connectForm);
    connectLayout->addLayout(connectButtonBox);

    mainLayout->addWidget(myNameLabel);
    mainLayout->addWidget(listenGroupBox);
    mainLayout->addWidget(connectGroupBox);


    setLayout(mainLayout);
    setWindowTitle("TicTacToeNetwork");

    setDefaultFormValues();


    // Init network
    network = new Network(this, myName);
    connect(this, &MainWindow::listenerStartRequested, network, &Network::startListener);
    connect(this, &MainWindow::listenerStopRequested, network, &Network::handleStopListener);
    connect(network, &Network::listenerStarted, this, &MainWindow::handleListenerStarted);
    connect(network, &Network::listenerStopped, this, &MainWindow::handleListenerStopped);
    connect(this, &MainWindow::newConnectionRequested, network, &Network::openNewConnection);
    connect(network, &Network::establishedNewConnection, this, &MainWindow::handleNewConnection);
    connect(network, &Network::errorOccured, this, &MainWindow::showErrorMessage);
}

MainWindow::~MainWindow() {}

/**
 * @brief MainWindow::handleListenClick notifies Listener to start listening when "Listen" button is pressed.
 */
void MainWindow::handleListenClick() {
    QString port = listenPortEdit->text();
    emit listenerStartRequested(port);
}

/**
 * @brief MainWindow::handleListenerStarted changes listening status when received confirmation from Listener.
 * @param port port on which Listener is listening.
 */
void MainWindow::handleListenerStarted(QString port)
{
    listenStatusLabel->setText("Listening (localhost:" + port + ")");
    startListenButton->setDisabled(true);
    stopListenButton->setDisabled(false);
}

/**
 * @brief MainWindow::handleListenerStarted changes listening status when received signal from Listener.
 */
void MainWindow::handleListenerStopped()
{
    listenStatusLabel->setText("Down");
    startListenButton->setDisabled(false);
    stopListenButton->setDisabled(true);
}

/**
 * @brief MainWindow::showErrorMessage displays error message to user.
 * @param errorMessage message to display.
 */
void MainWindow::showErrorMessage(QString errorMessage)
{
    QMessageBox::critical(this, "Error", errorMessage);
}

/**
 * @brief MainWindow::handleNewConnection displays waiting message or game request according to role.
 * @param provider game provider on game thread which should be handled.
 * @param opponentName name of the opponent.
 */
void MainWindow::handleNewConnection(GameProvider *provider, QString opponentName)
{
    if(provider->getConnectionRole() == GameProvider::INITIATED) {
        WaitingMessageBox *messageBox = new WaitingMessageBox(this);
        connect(provider, &GameProvider::opponentAcceptedRequest, messageBox, &WaitingMessageBox::handleAccept);
        connect(provider, &GameProvider::opponentAcceptedRequest, this, [=]() {associateWindowWithGame(provider, opponentName);});
        connect(provider, &GameProvider::opponentRejectedRequest, messageBox, &WaitingMessageBox::handleReject);
        messageBox->show();
    } else {
        GameRequestMessageBox *gamePrompt = new GameRequestMessageBox(this, opponentName);
        connect(gamePrompt, &GameRequestMessageBox::userAcceptedRequest, provider, &GameProvider::acceptGameRequest);
        connect(gamePrompt, &GameRequestMessageBox::userAcceptedRequest, this, [=]() {associateWindowWithGame(provider, opponentName);});
        connect(gamePrompt, &GameRequestMessageBox::userRejectedRequest, provider, &GameProvider::rejectGameRequest);
        gamePrompt->show();
    }
}

/**
 * @brief MainWindow::associateWindowWithGame if game got accepted, assosiates game window with coresponding thread.
 * @param provider game provider on game thread which should be handled.
 * @param opponentName name of the opponent.
 */
void MainWindow::associateWindowWithGame(GameProvider *provider, QString opponentName)
{
    GameWindow *window = new GameWindow(this, provider->getConnectionRole() == GameProvider::INITIATED, opponentName);
    connect(window, &GameWindow::makeMove, provider, &GameProvider::sendMove);
    connect(window, &GameWindow::gameStopped, provider, &GameProvider::dropConnection);
    connect(provider, &GameProvider::gameStarted, window, &GameWindow::gameStarted);
    connect(provider, &GameProvider::moveReceived, window, &GameWindow::moveReceived);
    connect(provider, &GameProvider::showMessage, window, &GameWindow::showMessage);
    connect(provider, &GameProvider::errorOccured, window, &GameWindow::fatalError);
    connect(provider, &GameProvider::connectionClosed, window, &GameWindow::closeWindow);
    connect(this, &MainWindow::stopAllGamesRequested, provider, &GameProvider::dropConnection);
    window->show();
}

/**
 * @brief MainWindow::connectToOpponent connects to opponent at given IP and port.
 */
void MainWindow::connectToOpponent()
{
    QString ip = connectAddressEdit->text();
    QString port = connectPortEdit->text();
    emit newConnectionRequested(ip, port);
}

/**
 * @brief MainWindow::setDefaultFormValues sets default values to selected form fields.
 */
void MainWindow::setDefaultFormValues() {
    listenPortEdit->setText(tr("65222"));
    connectAddressEdit->setText(tr("127.0.0.1"));
    connectPortEdit->setText(tr("65222"));
}

/**
 * @brief MainWindow::closeEvent displays exit confirmation message.
 * @param event close event.
 */
void MainWindow::closeEvent(QCloseEvent *event) {
    event->ignore();
    if (QMessageBox::Yes == QMessageBox::question(this, "Exit confirmation", "Closing TicToeNetwork window. All running games will be stopped. Contiture?")) {
        emit listenerStopRequested();
        emit stopAllGamesRequested();
        event->accept();
    }
}

