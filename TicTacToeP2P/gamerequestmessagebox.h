#ifndef GAMEREQUESTMESSAGEBOX_H
#define GAMEREQUESTMESSAGEBOX_H

#include <QMessageBox>
#include <QObject>
#include <QPushButton>

class GameRequestMessageBox : public QMessageBox
{
    Q_OBJECT
private:
    QPushButton *yesButton;
    QPushButton *noButton;

public:
    GameRequestMessageBox(QWidget *parent, QString opponentName);

signals:
    void userAcceptedRequest();
    void userRejectedRequest();
};

#endif // GAMEREQUESTMESSAGEBOX_H
