#ifndef GAMEPROVIDER_H
#define GAMEPROVIDER_H

#include "gamewindow.h"
#include "messagehandler.h"
#include "socket.h"

#include <QObject>

class GameProvider : public QObject
{
    Q_OBJECT

public:
    enum ConnectionRole {INITIATED, ACCEPTED};

    GameProvider(Socket otherSocket, ConnectionRole role, QString name);
    GameProvider();
    ~GameProvider();

    void receiveMessage();
    ConnectionRole getConnectionRole() { return connectionRole; }

public slots:
    void runGame();
    void dropConnection();
    void acceptGameRequest();
    void rejectGameRequest();
    void sendMove(char pos);
signals:
    void establishedNewConnection(GameProvider *provider, QString opponentName);
    void opponentAcceptedRequest();
    void opponentRejectedRequest();
    void gameStarted();
    void moveReceived(char pos);
    void connectionClosed();
    void showMessage(QString message);
    void errorOccured(QString reason);
    void unblockWaiting();

private:
    bool isDone = false;
    bool isMyTurn;
    bool isGameAccepted = false;
    Socket opponentSocket;
    ConnectionRole connectionRole;
    QString myName;
    QString opponentName;
    QString closeReason = "";

    MessageHandler messageHandler;

    void finalize();
};

#endif // GAMEPROVIDER_H
