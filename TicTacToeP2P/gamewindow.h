#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "gamewidget.h"
#include "socket.h"

#include <QDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QObject>

class GameWindow : public QDialog
{
    Q_OBJECT
public:
    GameWindow(QWidget *parent, bool isMyTurn, QString opponentName);
    void accept() override;
private:
    QGroupBox *scoreGroupBox;
    QPushButton *quitButton;
    QLabel *myScoreLabel;
    QLabel *opponentScoreLabel;
    QLabel *gameStatusLabel;
    GameWidget *gameWidget;

    int myScore = 0;
    int opponentScore = 0;
    QString opponentName;

signals:
    void makeMove(char pos);
    void moveReceived(char pos);
    void gameStopped();

public slots:
    void gameStarted();
    void gameEnded(int gameState);
    void showMessage(QString message);
    void fatalError(QString errorMessage);
    void closeWindow();
    void closeGame();
    void setGameStatus(QString status);
};

#endif // GAMEWINDOW_H
