#include "gamerequestmessagebox.h"


/**
 * @brief GameRequestMessageBox::GameRequestMessageBox QMessageBox whith question whether user accepts or rejects opponent's game request.
 * @param parent parent widget.
 * @param opponentName name of opponent.
 */
GameRequestMessageBox::GameRequestMessageBox(QWidget *parent, QString opponentName) : QMessageBox(parent)
{
    setWindowTitle("New game request");
    setIcon(Question);
    setText("New game request received from " + opponentName + ". Accept?");
    yesButton = addButton("Yes", QMessageBox::YesRole);
    noButton = addButton("No", QMessageBox::NoRole);
    connect(yesButton, &QPushButton::clicked, this, &GameRequestMessageBox::userAcceptedRequest);
    connect(noButton, &QPushButton::clicked, this, &GameRequestMessageBox::userRejectedRequest);
}
