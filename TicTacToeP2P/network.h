#ifndef NETWORK_H
#define NETWORK_H

#include "gameprovider.h"
#include "gamewidget.h"
#include "gamewindow.h"
#include "socket.h"

#include <QThread>
#include <QObject>



class Network : public QObject
{
    Q_OBJECT
    QThread listenThread;
public:
    Network(QObject *parent = nullptr, QString myName="");
    ~Network();
public slots:
    void handleStopListener();
    void openNewConnection(QString ipAdress, QString port);
    void handleGameRequest(Socket opponentSocket);

    void handleListenerError();
    void handleListenerStarted(QString port);
    void handleListenerStopped();
private:
    Socket socket;
    QString listeningPort;
    QString myName;
    QList<QThread*> createdThreads;
    bool isListening = false;

    void startGameThread(GameProvider *provider);

signals:
    void startListener(QString port);
    void stopListener();
    void listenerStarted(QString port);
    void listenerStopped();
    void connectionRequestReceived(Socket socket);
    void errorOccured(QString errorMessage);
    void establishedNewConnection(GameProvider *provider, QString opponentName);
};

#endif // NETWORK_H
