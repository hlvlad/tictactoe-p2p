#pragma once

struct BoardState {
    int state;
    int index = 0;

    BoardState(int state, int index) {
        this->state = state;
        this->index = index;
    }

    BoardState(int state) {
        this->state = state;
    }
};

class Board
{
public:
    Board();
    enum : char {Empty = '-', Cross = 'X', Nought = 'O'};
    enum : char {WinRow, WinColumn, WinDiagonal, Tie, None};
    void clear();
    void setSquare(int location, int player);
    void unsetSquare(int location);
    int getSquare(int location);
    void evaluate();
    BoardState getState();
private:
    char board[9];
    BoardState boardState;
};
