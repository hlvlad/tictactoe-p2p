#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include "socket.h"


class MessageHandler
{
public:
    enum : char { MOVE = 'm', DISCONNECT = 'd', NAME = 'n', ACCEPT = 'a', REJECT = 'r'};
    MessageHandler(Socket socket);
    void send(int type);
    void sendName(QString name);
    void sendMove(const char move);
    void receive();
    char receiveMove();
    char receiveMessageType();
    QString receiveName();
    void sendDisconnect();
    void sendSignal(const char signal);
private:
    Socket socket;
};

#endif // MESSAGEHANDLER_H
