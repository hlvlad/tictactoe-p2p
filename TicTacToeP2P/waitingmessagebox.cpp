#include "waitingmessagebox.h"

WaitingMessageBox::WaitingMessageBox(QWidget *parent) : QMessageBox(parent)
{
    setText("Waiting for opponent to accept request...");
    setStandardButtons(QMessageBox::Cancel);
}

void WaitingMessageBox::handleAccept()
{
    this->done(0);
}

void WaitingMessageBox::handleReject()
{
    QMessageBox::information(this, "Info", "Opponent rejected game request.");
    this->done(0);
}
